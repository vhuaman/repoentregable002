package vhf.entregable2.mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechuEntregables2Application {

	public static void main(String[] args) {
		SpringApplication.run(TechuEntregables2Application.class, args);
	}

}
