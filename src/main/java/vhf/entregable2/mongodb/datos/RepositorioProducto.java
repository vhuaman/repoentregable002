package vhf.entregable2.mongodb.datos;

import vhf.entregable2.mongodb.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String>
{
}
