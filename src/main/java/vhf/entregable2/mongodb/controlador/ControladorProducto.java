package vhf.entregable2.mongodb.controlador;

import vhf.entregable2.mongodb.modelo.Producto;
import vhf.entregable2.mongodb.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")
public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;
//parte 1
    @GetMapping()
    public List<Producto> obtenerProductos() {
        return this.servicioProducto.obtenerProductos();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Producto agregarProducto(@RequestBody Producto producto){
        return this.servicioProducto.crearProducto(producto);
    }
//parte 2
   @GetMapping("/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable String id){
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //@PutMapping("/{id}")
    @PutMapping()
    public void actualizarProducto(@RequestBody Producto productoModificado) {
        this.servicioProducto.actualizarProducto(productoModificado);
       //  throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProductoPorId(@PathVariable String id){
        this.servicioProducto.borrarProductoPorId(id);
    }

    @PatchMapping("/{id}")
    public void actualizarProductoPorId(@PathVariable String id, @RequestBody Producto productoModificado){
        productoModificado.setId(id);
        this.servicioProducto.actualizarProducto(productoModificado);
    }

}
